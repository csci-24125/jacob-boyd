# Lab 1 - Jacob Boyd
This Lab is an introduction to Git. We will learn how to use the workflows of git. 
## Big git no-nos
Never commit directly to the main branch in a repo
## About Me
I love to play golf and go fishing. Before transferring
to Columbus State I attended Wittenberg University and
played baseball there. I am eager to learn in this class.
## Proposed final project
A web site to sell recycled golf balls. the user can browse the site to see what golf balls are for sale
and add them to their cart. Once they hit check out it will ask them to sign in to their account. 
The acccount will be able to hold all purchases and user information to speed up check out. The user would be able to alter their account information while logged in. 
Some challenges ahead will be incorparating database to hold all the information just because ive never done so. 
