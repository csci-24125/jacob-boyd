<?php
class Article {
    // parameters
    public $conn;
    public $articleId;
    public $title;
    public $author;
    public $content;
    public $publishDate;
    public $isPublished;
    public $primaryImage;
    public $imageTitle;


    function __construct($conn, $articleInfo) {
        $this->conn = $conn;
        $this->articleId = $articleInfo['articleId'];
        $this->title = $articleInfo['title'];
        $this->author = $articleInfo['author'];
        $this->content = $articleInfo['content'];
        $this->publishDate = $articleInfo['publishDate'];
        $this->isPublished = $articleInfo['isPublished'];
        $this->primaryImage = $articleInfo['primaryImage'];
        $this->imageTitle = $articleInfo['imageTitle'];

    }

    function __destruct() { }
    static function getArticlesFromDb($conn, $numArticles = 20,$onlyPublished=true) {
        if ($onlyPublished) {
            $selectArticles = "SELECT articles.*, users.fullName as author
            FROM Articles
            LEFT JOIN (users) ON users.userId=articles.authorId
            WHERE articles.isPublished=true
            ORDER BY articles.publishDate DESC
            LIMIT :numArticles";
              } else {
            $selectArticles = "SELECT articles.*, users.fullName as author
            FROM Articles
            LEFT JOIN (users) ON users.userId=articles.authorId
            ORDER BY articles.publishDate DESC
            LIMIT :numArticles";
        }
        $stmt = $conn->prepare($selectArticles);
        $stmt->bindParam(':numArticles', $numArticles, PDO::PARAM_INT);
        $stmt->execute();
       
        $articleList = array();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        foreach($stmt->fetchAll() as $listRow) {
            $article = new Article($conn, $listRow);
            $articleList[] = $article;
        }
    
        return $articleList;
    }
 
    static function getArticleById($conn, $articleId) {
        $selectArticles = "SELECT articles.*, users.fullName as author
        FROM Articles LEFT JOIN (users)
        ON users.userId=articles.authorId
        WHERE articles.articleId=:articleId";

        
        $stmt = $conn->prepare($selectArticles);
        $stmt->bindParam(':articleId', $articleId, PDO::PARAM_INT);
        $stmt->execute();
   
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        foreach($stmt->fetchAll() as $listRow) {
            $article = new Article($conn, $listRow);
        }

        return $article;

    }
    function createArticle() {
        $insert = "INSERT INTO articles
            (publishDate, isPublished, title, content, authorId,primaryImage,imageTitle)
            VALUES
            (:publishDate, :isPublished, :title, :content, :authorId,:primaryImage, :imageTitle)";
        $stmt = $this->conn->prepare($insert);
        $stmt->bindParam(':publishDate', $this->publishDate);
        $stmt->bindParam(':isPublished', $this->isPublished, PDO::PARAM_BOOL);
        $stmt->bindParam(':title', $this->title);
        $stmt->bindParam(':content', $this->content);
        $stmt->bindParam(':authorId', $this->author, PDO::PARAM_INT);
        $stmt->bindParam(':primaryImage', $this->primaryImage);
        $stmt->bindParam(':imageTitle', $this->imageTitle);
        $stmt->execute();
    }

    function deleteArticle() {
        $delete = "DELETE FROM articles WHERE articleId=:articleId";
        $stmt = $this->conn->prepare($delete);
        $stmt->bindParam(':articleId', $this->articleId, PDO::PARAM_INT);
        $stmt->execute();
    }

    function updateArticle() {
        $currentDate = date('Y-m-d');
        $update = "UPDATE articles SET
            title=:title,
            isPublished=:isPublished,
            content=:content,
            publishDate=:publishDate,
            primaryImage=:primaryImage,
            imageTitle=:imageTitle
            WHERE articleId=:articleId";

        $stmt = $this->conn->prepare($update);
        $stmt->bindParam(':articleId', $this->articleId, PDO::PARAM_INT);
        $stmt->bindParam(':title', $this->title);
        $stmt->bindParam(':isPublished', $this->isPublished, PDO::PARAM_BOOL);
        $stmt->bindParam(':content', $this->content);
        $stmt->bindParam(':publishDate', $currentDate);
        $stmt->bindParam(':primaryImage', $this->primaryImage);
        $stmt->bindParam(':imageTitle', $this->imageTitle);
        $stmt->execute();


    
}
