<?php
include("../includes/navbar.php");
$articles = Article::getArticlesFromDb($conn,20,false);
//if (!isset($_SESSION['username'])) {
   // header("Location: 404.php");
  //}
  ?>
  

 
<div class="container">
<div class="row">
    <div class="d-flex justify-content-center">
      <a class='btn btn-success' href='createArticle.php'>Create New Article</a>
    </div>
  </div>

  <?php
      foreach ($articles as $article) {
      $articleId = $article->articleId;
  ?>
    <div class="row">
      <div class="col-12 listing-wrapper <?php echo ($article->isPublished ? '' : 'unpublished') ?>">

          <div class="row">
            <div class="col-12 col-md-7">
              <a class="" href="articlePage.php?articleId=<?php echo $articleId ?>">
                <span><?php echo $article->title ?></span>
              </a>
            </div>
            <div class="col-12 col-md-5 text-end">
              <a class='btn btn-success' href='editArticle.php?editArticleId=<?php echo $articleId ?>'>Edit</a>
              <a class='btn btn-danger' href='deleteArticle.php?deleteArticleId=<?php echo $articleId ?>'>Delete</a>
            </div>
          </div>
      </div>
    </div>
  <?php
    }
  ?>
</div>
