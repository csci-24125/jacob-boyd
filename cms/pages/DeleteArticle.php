<?php
include("../includes/navbar.php");
 
$articleId = $_GET['deleteArticleId'];
 
if (isset($articleId)) {
  try {
    $article = Article::getArticleById($conn, $articleId);
  } catch(Exception) {
    header("Location: ArticleListing.php");
  }
} else {
  header("Location: ArticleListing.php");
}
 
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $article->deleteArticle();
  header("Location: ArticleListing.php");
}
?>
 
<div class="container">
  <div class="row justify-content-center text-center">
    <div class="col-md-10 col-lg-8">
      <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <label for="submit">Are you sure you want to delete this article?</label>
        <input type="submit" class="btn btn-danger" value="Delete">
      </form>
    </div>
    <div class="col-md-10 col-lg-8">
      <a href="ArticleListing.php" class="btn btn-success">Cancel</a>
    </div>
  </div>
</div>
