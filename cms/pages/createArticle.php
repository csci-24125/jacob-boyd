<?php
include("../includes/navbar.php");
$imageErr = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  print_r($_FILES["fileToUpload"]);
  $kbFileSize = $_FILES["fileToUpload"]["size"] / 1000000;
    if ($kbFileSize > 10) {
        $imageErr = "Your file is too large. Max file size is 10 MB. Yours was $kbFileSize kb";
    }

    $title = clean_input($_POST["title"]);
    $content = clean_input($_POST["content"]);
    $primaryImage = file_get_contents($_FILES['fileToUpload']['tmp_name']);  
    $imageTitle = htmlspecialchars($_FILES["fileToUpload"]["name"]);
  
    $isPublished = false;
    if (isset($_POST['publish'])) {
      $isPublished = true;
    }
   
    if (!empty($title) && !empty($content)) {
      $authorId = getUserId($conn, $_SESSION['username']); // this function doesn't exist yet
      $publishDate = date('Y-m-d');
   
      $articleInfo = array(
        "articleId" => "",
        "publishDate" => $publishDate,
        "isPublished" => $isPublished,
        "title" => $title,
        "content" => $content,
        "author" => $authorId,
        "primaryImage" => $primaryImage,
      "imageTitle" => $imageTitle

      );
   
      $article = new Article($conn, $articleInfo);
      $article->createArticle(); // this method doesn't exist yet
      header("Location: ArticleListing.php");
    }
  }
  ?>
  

 
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10 col-lg-8 col-xl-7">
      <form enctype="multipart/form-data" method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <div class="form-group">
          <label for="title">Title</label>
          <span class="error">*<br>
          <input type="text" class="form-control" name="title" id="title" required>
        </div>
        <div class="form-group">
            <label for="fileToUpload">Select image to upload:</label>
            <input type="file" name="fileToUpload" id="fileToUpload" required>
            <span class="error">* <?php echo $imageErr;?></span><br>
        </div>

        <div class="form-group">
          <label for="content">Content</label>
          <span class="error">*<br>
          <textarea rows="10" class="form-control" name="content" id="content" required></textarea>
        </div>
        <div class="form-group">
          <label for="publish">Publish</label>
          <input type="checkbox" id="publish" name="publish">
        </div>
        <input type="submit" class="btn btn-primary" value="Submit">
    </form>    
    </div>
  </div>
</div>
