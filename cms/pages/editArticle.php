<?php
include("../includes/navbar.php");
 
if (isset($_GET['editArticleId'])) {
  try {
    $article = Article::getArticleById($conn, $_GET['editArticleId']);
  } catch(Exception) {
    header("Location: ArticleListing.php");
  }
} else {
  header("Location: ArticleListing.php");
}
$imageErr = "";
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $mbFileSize = $_FILES["fileToUpload"]["size"] / 1000000;
  if ($mbFileSize > 10) {
    $imageErr = "Your file is too large. Max file size is 10MB. Yours was $mbFileSize MB";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $title = clean_input($_POST["title"]);
    $content = clean_input($_POST["content"]);
    $primaryImage = file_get_contents($_FILES['fileToUpload']['tmp_name']);
    $imageTitle = htmlspecialchars($_FILES["fileToUpload"]["name"]);
  
    $isPublished = false;
    if (isset($_POST['publish'])) {
      $isPublished = true;
    }
   
    if (!empty($title) && !empty($content)) {
        $article->title = $title;
        $article->content = $content;
        $article->isPublished = $isPublished;
        $article->primaryImage = $primaryImage;
        $article->imageTitle = $imageTitle;
        $article->updateArticle();
        header("Location: ArticleListing.php");
    
    }
  }
  
?>
 
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10 col-lg-8 col-xl-7">
      <form enctype="multipart/form-data" method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
        <div class="form-group">
          <label for="author">Author</label>
          <span class="error">*<br>
          <input type="text" class="form-control" name="author" id="author" disabled value="<?php echo $article->author ?>" required>

        </div>
        <div class="form-group">
          <label for="title">Title</label>
          <span class="error">*<br>
          <input type="text" class="form-control" name="title" id="title" value="<?php echo $article->title?>" required>
        </div>
        <div class="row">
  <div class="col-9 col-md-6">
    <div class="form-group">
        <label for="fileToUpload">Select image to upload:</label>
        <input type="file" name="fileToUpload" id="fileToUpload">
        <span class="error">* <?php echo $imageErr;?></span><br>
    </div>
  </div>
  <div class="col-3 col-md-6">
    <?php if (!empty($article->primaryImage)) { ?>
      <img class="d-block w-100" src='data:image/jpeg;base64,<?php echo base64_encode( $article->primaryImage )?>' />
    <?php } ?>
  </div>
</div>

        <div class="form-group">
            <label for="fileToUpload">Select image to upload:</label>
            <input type="file" name="fileToUpload" id="fileToUpload">
            <span class="error">* <?php echo $imageErr;?></span><br>
        </div>

        <div class="form-group">
          <label for="content">Content</label>
          <span class="error">*</span><br>
          <textarea rows="10" class="form-control" name="content" id="content" required><?php echo $article->content?></textarea>
        </div>
        <div class="form-group">
          <label for="publish">Publish</label>
          <input type="checkbox" id="publish" name="publish" <?php echo ($article->isPublished ? "checked" : "") ?>>
        </div>
        <input type="submit" class="btn btn-primary" value="Submit">
    </form>    
    </div>
  </div>
</div>
