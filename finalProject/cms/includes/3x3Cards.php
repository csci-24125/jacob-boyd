<head><style>
  .card{
    color:black;
    background-color:#5BC85B;
  }
</style></head>

<div class="container">
  <div class="row">
    <?php
        $data = Product::getProductsFromDb($conn, 3);
        foreach ($data as $products) {
    ?>
      <div class="col-4">
      <a class="card-wrapper"
href="../pages/displayProduct.php?productId=<?php echo $products->productId ?>"> 

        <div class="card">
            <h2><?php echo $products->productName ?></h2>
            <p>$<?php echo $products->price ?></p>
            <img src="../images/<?=$products->img?>" width="100" height="100" alt="<?=$products->productName?>">
            <p>SKU:<?php echo $products->productId ?></p>
        </div>
      
        </a>
      </div>
    <?php
        }
    ?>
  </div>
</div>
