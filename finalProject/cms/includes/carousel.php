<!DOCTYPE html>
<html lang="en">
<head>
  
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script> 
</head>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
  
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  
  <div class="carousel-inner">
    <div class="item active">
      <img src="../images/putt.jpg" alt="Ball falling in hole">
      <div class="carousel-caption">
        <h2 style="color:black">Golf Balls</h2>
        <p style="color:black">The best Reused Golf Balls around!</p>
      </div>
    </div>

    <div class="item">
      <img src="../images/brands.jpg" alt="asorted golfballs">
      <div class="carousel-caption">
        <h3 style="color:black">Name Brand</h3>
        <p style="color:black">Wide selection of all top brands!</p>
      </div>
    </div>

    <div class="item">
      <img src="../images/packaged.jpg" alt="boxes">
      <div class="carousel-caption">
        <h3 style="color:black">Buy a Bundle!</h3>
        <p style="color:black">Sold in Sleeves and Boxes!</p>
      </div>
    </div>
  </div>

  
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>