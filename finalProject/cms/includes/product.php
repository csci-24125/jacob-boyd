<?php
class Product{
    public $conn;
    public $productId;
    public $productNumber;
    public $productName;
    public $price;
    public $img;
    

    function __construct($conn, $productInfo) {
        $this->conn = $conn;
        $this->productId = $productInfo['productId'];
        $this->productNumber = $productInfo['productNumber'];
        $this->productName = $productInfo['productName'];
        $this->price = $productInfo['price'];
        $this->img=$productInfo['img'];
        
    }

    function __destruct() { }

    static function getProductsFromDb($conn, $numProducts = 30) {
        $selectProducts = "SELECT product.* FROM product Limit :numProducts";
        $stmt = $conn->prepare($selectProducts);
        $stmt->bindParam(':numProducts', $numProducts, PDO::PARAM_INT);
        $stmt->execute();
       
        $productList = array();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        foreach($stmt->fetchAll() as $listRow) {
            $product = new Product($conn, $listRow);
            $productList[] = $product;
        }
    
        return $productList;
    }
    static function getProductById($conn, $productId) {
        $selectProducts = "SELECT product.*, users.fullName as author
        FROM Articles LEFT JOIN (users)
        ON users.userId=articles.authorId
        WHERE articles.articleId=:articleId";

        
        $stmt = $conn->prepare($selectArticles);
        $stmt->bindParam(':articleId', $articleId, PDO::PARAM_INT);
        $stmt->execute();
   
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        foreach($stmt->fetchAll() as $listRow) {
            $article = new Article($conn, $listRow);
        }

        return $article;

    }
}