<?php
include("../includes/navbar.php");
$product = Product::getProductsFromDb($conn);
if (isset($_GET['productId'])) {
    $stmt = $conn->prepare('SELECT * FROM product WHERE productId = ?');
    $stmt->execute([$_GET['productId']]);
    $products = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$products) {
        exit('Product does not exist!');
    }
} else {
    exit('Product does not exist!');
}


?>



<div class="product content-wrapper">
     <img src="../images/<?=$products['img']?>" width="300" height="300" alt="<?=$products['productName']?>">
    <div>
        <h1 class="name"><?=$products['productName']?></h1>
        <span class="price">
            &dollar;<?=$products['price']?>
        </span>
        
        <form action="cart.php" method="post">
            <input type="number" name="quantity" value="1" min="1">
            <input type="hidden" name="productId" value="<?=$products['productId']?>">
            <input type="submit" value="Add To Cart">
        </form>
    </div>
</div>