<?php
include("../includes/navbar.php");
$product = Product::getProductsFromDb($conn);
if (!isset($_SESSION['username'])) {
   header("Location: Login.php");
}
?>

<div class="placeorder content-wrapper">
    <h1>Your Order Has Been Placed</h1>
    <p>Thank you for ordering with us, we'll contact you by email with your order details.</p>
</div>