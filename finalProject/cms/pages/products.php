<?php
include("../includes/navbar.php");
$product = Product::getProductsFromDb($conn);
  ?>
<head><style>
  .card{
    color:black;
    background-color:#5BC85B;
  }
</style></head>
<h1>Products</h1>
<div class="container" >
  <div class="row">
    <?php
        $data = Product::getProductsFromDb($conn,);
        foreach ($data as $products) {
    ?>
      <div class="col-3">
      <a class="card-wrapper"
href="../pages/displayProduct.php?productId=<?php echo $products->productId ?>"> 

        <div class="card">
            <h2><?php echo $products->productName ?></h2>
            <img src="../images/<?=$products->img?>" width="100" height="100" alt="<?=$products->productName?>">
            <p>$<?php echo $products->price ?></p>
            <p>SKU:<?php echo $products->productId ?></p>
        </div>
      
        </a>
      </div>
    <?php
        }
    ?>
  </div>
</div>