<?php
include("../includes/navbar.php");
$reviewBoxErr = "";
  $starErr = "";
  $stars = "";
  $review = "";
  $imageErr = "";


  
  $conn = connect_to_db("finalprojectjacobboyd");

  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["review"])){
        $reviewBoxErr = "A review is required";
        
    }else{
        $review = clean_input($_POST["review"]);
    }

    $mbFileSize = $_FILES["fileToUpload"]["size"] / 1000000;
    if ($mbFileSize > 10) {
      $imageErr = "Your file is too large. Max file size is 10MB. Yours was $mbFileSize MB";
    }
    if (empty($_POST["stars"])){
        $starErr = "Select a star review";
    }else{
        $stars = clean_input($_POST["stars"]);
        
    }
    if(!empty($_FILES['fileToUpload']['tmp_name'])){
    $primaryImage = file_get_contents($_FILES['fileToUpload']['tmp_name']);  
    $imageTitle = htmlspecialchars($_FILES["fileToUpload"]["name"]);
  }
    if(!empty($stars) and !empty($review)){
        try{
        
        $insert = "INSERT INTO Reviews (reviewText,numStars,primaryImage, imageTitle)
        VALUES (:review, :stars,:primaryImage, :imageTitle)";
        $stmt = $conn->prepare($insert);
        $stmt->bindParam(':review', $review);
        $stmt->bindParam(':stars', $stars);
        $stmt->bindParam(':primaryImage', $primaryImage);
        $stmt->bindParam(':imageTitle', $imageTitle);
        $stmt->execute();
        }    
        catch(PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
          }
    }
}
  function printreviewList($conn) {
    $selectItem = "SELECT * FROM Reviews";
    $stmt = $conn->prepare($selectItem);
    $stmt->execute();
 
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    echo "<div class='reviewListItem row'>";
    foreach($stmt->fetchAll() as $listRow) {
       
        $reviewId = $listRow['reviewId'];
        $reviewText = $listRow['reviewText'];
        $reviewStars = $listRow['numStars'];
        $primaryImage = $listRow['primaryImage'];
        $imageTitle =$listRow['imageTitle'];
        echo "<p class='col-2 '>$reviewText</p>";
       
       
        for($i = 0;$i<$reviewStars;$i++){
            echo "★";
        }
         echo "</p> <p>";
            if (!empty($primaryImage)) {
                ?><img class='col-2' width= '400' src='data:image/jpeg;base64,<?php echo base64_encode( $primaryImage )?>' /><?php
              }  
              echo "</p>";
    }    
    
        echo "</div>";
    }
?>
<link rel="stylesheet" href="reviewList.css">
<div class="container" text-align:center>
<h1>Write a Review!</h1>
<style>
    .error {color: #FF0000;}
    </style>
    <p><span class="error">* required field</span></p>
<form enctype="multipart/form-data" method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
<label for="review">Write a review:</label>
<textarea type="textarea" name="review" id="review" required></textarea>
<span class="error">* <?php echo $reviewBoxErr;?></span><br>
<label for="stars"> Star Review: </label>
<input type="radio" name="stars" value="1">1 Stars
<input type="radio" name="stars" value="2">2 Stars
<input type="radio" name="stars" value="3">3 Stars
<input type="radio" name="stars" value="4">4 Stars
<input type="radio" name="stars" value="5">5 Stars
<span class="error" required>* <?php echo $starErr;?></span><br><br>
<div class="form-group">
            <label for="fileToUpload">Select image to upload:</label>
            <input type="file" name="fileToUpload" id="fileToUpload">
        </div>
<input type="submit" value="Submit"><br><br><br>

<h2>Reviews!</h2>
<div class = "contaner">
<?php printreviewList($conn); ?>
</body>
</html>