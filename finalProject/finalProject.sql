-- CREATE DATABASE finalProjectJacobBoyd;
use finalProjectJacobBoyd;

drop table if exists orders;
drop table if exists users;
drop table if exists product;


CREATE TABLE users (
    userId INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    userName VARCHAR(255),
    passwords varchar(255),
    address int,
    fullName varchar(255)
);

CREATE TABLE product(
productId INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
productNumber INT,
productName VARCHAR(255),
price decimal (4,2),
img varchar(255)
);

INSERT INTO product (productNumber,productName,price,img)
VALUES
(001,"Pro V1 Sleeve",25.99,"prov1sleeve.jpg"),
(002,"Noodle Box", 20.99,"noodlebox.jpg"),
(003,"Bridgestone Box",24.99,"bridgestonebox.jpg"),
(004,"Noodle Sleeve", 10.99,"noodlesleeve.jpg");

INSERT INTO users (username, passwords, address, fullName)
VALUES
("Jacobbo", "1234", 1, "Jacob Boyd"),
("darnellkin","4567",2, "Darnell King");

select * FROM product;
select * FROM users;
select * from reviews;
