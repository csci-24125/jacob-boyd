<?php
// echo "Hello world!";

$firstName = "Jacob";
$lastName = "Boyd" ;
$age = 20;
$currentYear = 2022; 
$isMarried = false ;

echo "$firstName $lastName </br>";
echo $firstName . " freaking " . $lastName . "</br>";
echo "I was born in " . $currentYear - $age . "</br>";
echo "I am married: $isMarried </br>"; //This will print a 1 or a 0 (or a blank), 1 means true, 0 (or nothing) means false

echo "My name is $firstName </br>"; // will print "My name is " + whatever you set $firstName equal to above

echo "$firstName blah $lastName </br>";
echo $firstName . " blah " . $lastName . "</br>"; // these will print the same thing.

$birthYear = $currentYear - $age;
echo $birthYear ."</br>";
echo $currentYear - $age ."</br>"; // these will print the same thing

$fullName = $firstName . " " . $lastName;
echo $fullName ."</br>";
echo "$firstName $lastName </br>"; // these will print the same thing

echo 8 % 3  ."</br>"; // = 2 ?
echo 15 % 4  ."</br>"; // = 3?
echo 4 % 9  ."</br>"; // = 4?
echo 7 % 2  ."</br>"; // = 1?

$generation = ""; // set generation equal to an empty string
if ($birthYear < 1965) {
    $generation = "Boomer";
} elseif ($birthYear >= 1965 && $birthYear < 1981) {
    $generation = "Gen X";
} elseif ($birthYear >= 1981 && $birthYear < 1996) {
    $generation = "Millennial";
} elseif ($birthYear >= 1996 && $birthYear < 2012) {
    $generation = "Gen Z";
} else {
    $generation = "Too young for a generational label";
}
echo $generation ."</br>";

