<?php

function MultiplyBy37($InputNumber){
    if(is_numeric($InputNumber)== false){
        throw new Exception("Is $InputNumber a number??"); 
    }
    return $InputNumber * 37;

}

try{
MultiplyBy37(7);
MultiplyBy37(3);
MultiplyBy37("Mayonnaise");
}catch(Exception $ex) {
    $message = $ex->getMessage();
    $file = $ex->getFile();
    $line = $ex->getTraceAsString();
    echo "$message $file $line";
} finally {
    echo "Process complete.";
}


function SumOfMultiples3and5($MaxValue){
    $sum = 0;
    if(is_numeric($MaxValue)== false){
        throw new Exception("Pass a natural number, $MaxValue is not!");
    }
    for($i=1;$i <$MaxValue;$i++){
        if($i % 3 ==0 || $i % 5 ==0){
            $sum += $i;
            
        }
    }

    echo $sum;

}

try{
     SumOfMultiples3and5(10);
    echo "</br>";
     SumOfMultiples3and5(1000);
}catch(Exception $ex) {
        $message = $ex->getMessage();
        $file = $ex->getFile();
        $line = $ex->getTraceAsString();
        echo "$message $file $line";
    } 
    

