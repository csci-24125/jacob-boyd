<html>
<body>
    <h1>Fuel Calculator!</h1>
<?php
 $avgMiles = "";
 $mpg = "";
 $gas = "";
 $fuelPerks = "";
 $cost="";

 $avgMilesErr = "";
 $mpgErr = "";
 $gasErr = "";

 if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["avgMilesDriven"])){
        $avgMilesErr = "Average Miles Driven is required";
    }else{
        $avgMiles = clean_input($_POST["avgMilesDriven"]);
        if(!is_numeric($avgMiles)){
            $avgMilesErr = "Input must be a number";
            $avgMiles = "";
        }
    }

    if (empty($_POST["mpg"])){
        $mpgErr = "Miles per Gallon is required";
    }else{
        $mpg = clean_input($_POST["mpg"]);
        if(!is_numeric($mpg)){
            $mpgErr = "Input must be a number";
            $mpg = "";
        }
    }

    if (empty($_POST["gas"])){
        $gasErr = "Select a gas is required";
    }else{
        $gas = clean_input($_POST["gas"]);
        
    }
    $fuelPerks = clean_input($_POST["fuelperks"]);
    if(!is_numeric($fuelPerks)){
        $fuelPerks = "";
    }
    $cost=calculate_price($mpg,$avgMiles,$gas,$fuelPerks);
    if($cost == -1){
        $cost = "";
    }
    
 }

 function calculate_price($mpg,$avgMiles,$gas,$fuelPerks){
 if($mpg == "" || $avgMiles == "" || $gas == ""){
     return -1;
 }else{
     if($fuelPerks >= 100){
     $discount = floor($fuelPerks / 100);
     $gallons = $avgMiles / $mpg;
     $gas = $gas - ($discount *.1);
     $cost = $gallons * $gas ; 
     }else{
        $gallons = $avgMiles / $mpg;
        $cost = $gallons *  $gas ;
     }
     return $cost;

 }
 
}

 function clean_input($data) {
    $data = trim($data); 
    $data = stripslashes($data); 
    $data = htmlspecialchars($data);
    return $data;
  }

?>

<style>
    .error {color: #FF0000;}
    </style>
    <p><span class="error">* required field</span></p>


<form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
<label for="avgMilesDriven">Average Miles Driven:</label>
<input type="text" name="avgMilesDriven" id="avgMilesDriven">
<span class="error">* <?php echo $avgMilesErr;?></span><br><br>

<label for="mpg"> Miles Per Gallon:<label>
<input type="text" name="mpg" id="mpg">
<span class="error">* <?php echo $mpgErr;?></span><br><br>

<label for="gas"> Gas:</label>
<input type="radio" name="gas" value="1.89">87 octane ($1.89)
<input type="radio" name="gas" value="1.99">89 octane ($1.99)
<input type="radio" name="gas" value="2.09">92 octane ($2.09)
<span class="error">* <?php echo $gasErr;?></span><br><br>

<label for="fuelperks"> Amount of money spent this week (fuel perks)</label>
<input type="text" name="fuelperks" id="fuelperks"><br><br>


<input type="submit" value="Submit">
</body>


<p>gas cost: $<?php echo $cost; ?> <p>

</html>