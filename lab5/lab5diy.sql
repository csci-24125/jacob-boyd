USE lab5;

DROP TABLE IF EXISTS Golfer;

CREATE TABLE Golfer(
golferID INT auto_increment PRIMARY KEY,
golferName VARCHAR(255),
age INT,
score INT,
underPar BOOLEAN,
datePlayed date
);
 
 INSERT INTO Golfer(GolferName,age,score,underPar,datePlayed)
 Values("Dustin Johnson",24,74,false,"2019-5-23");
 INSERT INTO Golfer(GolferName,age,score,underPar,datePlayed)
 Values("Bryson DeChambeau",23,67,true,"2018-07-19");
INSERT INTO Golfer(GolferName,age,score,underPar,datePlayed)
 Values("Justin Thomas",27,70,true,"2016-5-02");
 INSERT INTO Golfer(GolferName,age,score,underPar,datePlayed)
 Values("Tiger Woods",31,65,true,"2016-6-16");
INSERT INTO Golfer(GolferName,age,score,underPar,datePlayed)
 Values("Jack Nickolas",27,71,false,"1990-3-12");


select * from Golfer;