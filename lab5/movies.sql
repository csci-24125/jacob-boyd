USE lab5;

DROP TABLE IF EXISTS movies;

CREATE TABLE movies(
movieID INT AUTO_INCREMENT PRIMARY KEY,
title varchar(255),
director varchar(255),
releaseDate date
);

INSERT INTO movies(title,director,releaseDate)
VALUES
("The Notebook","Nick Cassavetes","2004-06-25");
INSERT INTO movies(title,director,releaseDate)
VALUES
("Titanic","James Cameron","1997-12-19");

Select * from movies;
