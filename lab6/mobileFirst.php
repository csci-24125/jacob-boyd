<html>
    <?php
        include("../cms/includes/navbar.php");
        
    ?>
    <link rel="stylesheet" href="breakpoints.css">
<body>
    <h1>This is showing mobile first development</h1>
    <div class="container">
  <div class="row">
      <div class="col-12 col-md-4" style="background-color: azure;">
          <h2>Column 1</h2>
      </div>
      <div class="col-12 col-md-4" style="background-color: bisque;">
          <h2>Column 2</h2>
      </div>
      <div class="col-12 col-md-4" style="background-color: coral;">
          <h2>Column 3</h2>
      </div>
 </div>
 <div class="row">
      <div class="col-6 col-sm-3" style="background-color: azure;">
          <h2>Column 1</h2>
      </div>
      <div class="col-6 col-sm-3" style="background-color: bisque;">
          <h2>Column 2</h2>
      </div>
      <div class="col-6 col-sm-3" style="background-color: coral;">
          <h2>Column 3</h2>
      </div>
      <div class="col-6 col-md-3" style="background-color: azure;">
          <h2>Column 4</h2>
      </div>
  </div>

    </div>

</body>
</html>
