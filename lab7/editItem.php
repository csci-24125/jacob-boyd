<?php
  include("../cms/includes/navbar.php");
  
?>

<link rel="stylesheet" href="toDoList.css">
<div class='toDoList'>
  <form method="post" action="<?php htmlspecialchars($_SERVER["PHP_SELF"]);?>">
    <label for="toDo" >To Do:</label>
    <input type="text" name="toDo" id="toDo" value="<?php echo $_GET['editItem']?>"><br>
 
    <input type="checkbox" name="isDone" id="isDone" value="true">
    <label for="isDone">Is Done</label>
 
    <input type="submit" class="btn btn-primary" value="Submit">
  </form>


<?php

$toDoItem = $_GET['editItem'];
$isDone = "false";

$conn = connect_to_db("toDoList");


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (clean_input($_POST['toDo'])) {
        $toDoItem = clean_input($_POST['toDo']);
    }
    if (isset($_POST['isDone'])) {
        $isDone = 'true';
    }

    header("Location: http://localhost/jacob-boyd/lab7/todolist.php");
}

updateToDoListItem($conn,$_GET['editItemId'],$toDoItem,$isDone);

function updateToDoListItem($conn, $itemId,$toDoItem,$isDone) {
    $update = "UPDATE items
        SET 
        isComplete = :isComplete,
        toDoItem = :toDoItem
        WHERE itemId=:itemId";   
    $stmt = $conn->prepare($update);
    $stmt->bindParam(':isComplete',$isDone, PDO::PARAM_BOOL);
    $stmt->bindParam(':toDoItem',$toDoItem);
    $stmt->bindParam(':itemId', $itemId);
    $stmt->execute();
}