<?php

class CashRegisters{
    protected $amountInRegister;
    function __construct($amountInRegister) {
        $this->amountInRegister = $amountInRegister;
    }
    function __destruct(){}

    function get_amountInRegister(){
        return $this->amountInRegister;
    }
    function set_amountInRegister($amountInRegister){
        $this->amountInRegister=$amountInRegister;
    }
    function addMoney($amount){
        $this->amountInRegister = $this->amountInRegister + $amount;
    }
    function removeMoney($amount){
        if($amount<=$this->amountInRegister){
            
            $this->amountInRegister = $this->amountInRegister - $amount;
        }else{
            echo "user can not remove that much money.<br>";
        }
    }
}
