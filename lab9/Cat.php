<?php
include("animal.php");
class Cat extends Animal{
    public function amountOfTimeSpentSleeping(){
        $hoursSleeping = $this->age * 365 * 15;
        echo "$this->name has spent $hoursSleeping hours sleeping."; 
    }
}
$milo = new Cat("Milo",4);
$milo->amountOfTimeSpentSleeping();
echo "</br>";
