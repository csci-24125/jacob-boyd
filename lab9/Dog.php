
<?php
include("Animal.php");
class Dog extends Animal {
    public function convertAge() {
        $convertedAge = $this->age * 7;
        echo "The dog $this->name is $this->age years old, which is $convertedAge dog years!</br>";
    }
    public function dogAge() {
        $this->age = $this->multiplyAgeBy7();
        echo $this->age;
    }
    private function multiplyAgeBy7() {
        return $this->age * 7;
    }
}
$pochi = new Dog('Pochi', 7);
$pochi->convertAge();
