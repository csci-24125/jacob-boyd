<?php

include("PiggyBank.php");

$piggybank = new PiggyBank(20);
$CashRegister = new CashRegisters(20);
echo "piggy bank = $" . $piggybank->get_amountInRegister() . " Cash Register = $".$CashRegister->get_amountInRegister(). "<br>";
$piggybank->addMoney(10);
$CashRegister->addMoney(10);
echo "piggy bank = $" . $piggybank->get_amountInRegister() . " Cash Register = $".$CashRegister->get_amountInRegister(). "<br>";
$piggybank->removeMoney(5);
$CashRegister->removeMoney(5);
echo "piggy bank = $" . $piggybank->get_amountInRegister() . " Cash Register = $".$CashRegister->get_amountInRegister(). "<br>";
$piggybank->removeMoney(70);
$CashRegister->removeMoney(70);
echo "piggy bank = $" . $piggybank->get_amountInRegister() . " Cash Register = $".$CashRegister->get_amountInRegister(). "<br>";
$piggybank->breakBank();